# README #

This repository contains my solutions to problems provided by DailyCodingProblem
mailing list.

The principal language I use is C (-std=c99 unless some extensions are required).
If the problem provides pieces of code in other languages the solution for that
problem uses the given language.

For C programs each folder has a Makefile. Compile it with `make` or `make debug`.

## List of problems

1. [Find sum of 2 numbers (C)](src/one)
2. [Product of elements of array except itself (C)](src/two)
3. [Serialization/deserialization of binary tree (Python)](src/three)
4. [Lowest positive int not present in array (C)](src/four)
5. [Python closure (Python)](src/five)
6. [XOR linked list (C)](src/six)
7. [Count decodable string (C)](src/seven)
8. [Unival trees (C)](src/eight)
9. [Largest sum of non-adjacent numbers (C)](src/nine)
10. [Job scheduler (C)](src/ten)
11. [Find prefixed words in dictionary (Python)](src/eleven)
12. [Ways to climb a stair with given number of steps per move (C)](src/twelve)
13. [Longest substring with at max k distinct letters (C)](src/thirteen)
14. [Monte Carlo approach for pi approximation (C with OpenMP)](src/fourteen)
15. [Pick random element in a stream (C)](src/fifteen)
16. [Record last N order ids (C)](src/sixteen)
17. [Longest path in directory tree (C)](src/seventeen)
18. [Max of all subarrays of size k (C)](src/eighteen)
