#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "sort.h"

void copy_array(long *dst, long *src, unsigned int size)
{
	if ((src < dst && src + size >= dst) || (dst < src && dst + size >= src) || src == dst)
	{
		fprintf(stderr, "ERROR: memory overlap in copy_array()! Cannot copy memory.\n");
		exit(1);
	}
	memcpy(dst, src, sizeof(long)*size);
}

void merge(long *src, unsigned int begin, unsigned int middle, unsigned int end, long *dst)
{
	unsigned int i = begin, j = middle, k;

	for (k = begin; k < end; ++k)
	{
		if (i < middle && (j >= end || src[i] <= src[j]))
		{
			dst[k] = src[i++];
		}
		else
		{
			dst[k] = src[j++];
		}
	}

#ifdef __DEBUG__
	printf("begin=%u, end=%u ", begin, end);
	print_array(stdout, "merge", dst+begin, end-begin);
#endif
}

void split_merge(long *src, unsigned int begin, unsigned int end, long *working)
{
	unsigned int middle;
#ifdef __DEBUG__
	printf("begin=%u, end=%u ", begin, end);
	print_array(stdout, "split", working+begin, end-begin);
#endif

	if (end - begin < 2)
	{
		// Single element array, nothing to sort
		return;
	}

	middle = (end + begin) / 2;

	split_merge(working, begin, middle, src);
	split_merge(working, middle, end, src);

	merge(src, begin, middle, end, working);
}

void merge_sort(long *input, unsigned int size)
{
	long *tmp;
#ifdef __DEBUG__
	print_array(stdout, "Merge sort - array before sorting", input, size);
#endif

	tmp = malloc(sizeof(long)*size);
	if (tmp == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer in merge_sort().\n");
		exit(1);
	}

	copy_array(tmp, input, size);
	split_merge(tmp, 0, size, input);

	free(tmp);

#ifdef __DEBUG__
	print_array(stdout, "Merge sort - array after sorting", input, size);
#endif
}
