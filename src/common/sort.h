#ifndef __SORT_H__
#define __SORT_H__

#define sort merge_sort

/* void merge_sort(long *N, unsigned int size)
 * 
 * Sort all 'size' elements of the array 'N'
 * using merge_sort. */
void merge_sort(long *, unsigned int);

#endif
