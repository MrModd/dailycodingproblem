#ifndef _STRTOL_ERROR_HANDLING_
#define _STRTOL_ERROR_HANDLING_

/**
 * STRTOL_ERROR_HANDLING(char *nptr, char *endptr, int ret)
 *
 * Evaluate the success of the previous strtol operation
 * by checking the *nptr, the *endptr and the errno values returned
 * by the function.
 *
 * In case of errors exit with 'ret' error code.
 */
#define STRTOL_ERROR_HANDLING(nptr, endptr, ret) \
	if (nptr == endptr || !(nptr[0] != '\0' && *endptr == '\0') || errno != 0)	\
	{											\
		fprintf(stderr, "Error converting the input %s into a number.\n", nptr);	\
		if (errno != 0)									\
		{										\
			perror("strtol");							\
		}										\
		exit(ret);									\
	}

#endif
