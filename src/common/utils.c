#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/strtol_error_handling.h"
#include "utils.h"

void print_array(FILE *stream, const char *message, long *numbers, int size)
{
	int i;
	fprintf(stream, "%s:", message);
	for (i = 0; i < size; ++i)
	{
		fprintf(stream, " %ld", numbers[i]);
	}
	fprintf(stream, "\n");
}

void parse_array_long(long *output, char **args, int size)
{
	int i;
	char *endptr = 0; // Used for error checking on strtol()

	for(i = 0; i < size; ++i) {
		errno = 0;
		output[i] = strtol(args[i], &endptr, 10);
		STRTOL_ERROR_HANDLING(args[i], endptr, 99);
	}

#ifdef __DEBUG__
	print_array(stderr, "parse_array_long() parsed numbers", output, size);
#endif
}
