#ifndef __UTILS__
#define __UTILS__

/* Print an array of long in a given file stream
 * and prefixing it with a message */
void print_array(FILE *, const char *, long *, int);

/* Translate an array of strings encoding numbers into an array of long */
void parse_array_long(long *, char **, int);

#endif
