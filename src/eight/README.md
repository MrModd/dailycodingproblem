# Daily Coding Problem: Problem #8 [Easy]

This problem was asked by Google.

A unival tree (which stands for "universal value") is a tree where all
nodes under it have the same value.

Given the root to a binary tree, count the number of unival subtrees.

For example, the following tree has 5 unival subtrees:

```
   0
  / \
 1   0
    / \
   1   0
  / \
 1   1
```

## Solution

This problem is super easy to solve using recursion and since there are no
constraints on the complexity I'll stick with it :).

The recursive function takes one argument in input (the root of a tree) and
returns two values: the number of unival subtrees under the given root and
if at least one of the subtrees was not an unival tree.

Why we need this second flag returned is not immediately comprehensible by
the example given in the problem description, but with just a new node it
will be clear:

```
   0
  / \
 1   0
    / \
   1   0
  / \
 1   1
      \
       0
```

In this tree the node right-left child of the root is not anymore an unival
subtree because of the leaf 0 (its right-righ child).

The recursive visit of the tree is a Depth First Search (DFS) algorithm. You
start visiting the leaves and you go back to the root. The flag helps you
visualize the scenario when you started counting the unival subtrees and while
remounting to the source you find a node that is not an unival subtree.
From that node upwards there can't be any new unival subtree.

### Execution time

Execution time is the one of a DFS: `o(|V|+|E|)` where `|V|` is the
cardinality of the vertex and `|E|` the cardinality of the edges.

### Memory space

`o(|V|)` with `|V|` the vertex cardinality.
