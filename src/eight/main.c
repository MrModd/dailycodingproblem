#include <stdio.h>
#include <stdlib.h>
#include "node.h"

/* Compare the value field of nodes a and b. */
inline int equal_nodes_values(struct node *a, struct node *b)
{
	return a->value == b->value;
}

/* Count how many unival subtrees are there starting from the given root node.
 *
 * return_validity is set to 0 in case one subtree of the given node was not
 * an unival tree invalidating so the upstream subtrees. In case all the
 * subtrees are unival, then the return_validity value is unchanged after return.
 * If returns_validity is NULL, then the update is ignored.
 *
 * The return value is the count of unival subtrees found. */
unsigned long count_unival_trees(struct node *root, unsigned long *return_validity)
{
	unsigned long c = 0l, left_subtree, right_subtree, is_valid = 1l;

	if (root == NULL)
	{
		return 0l;
	}

	if (root->left_child == NULL && root->right_child == NULL)
	{
		// It's a leaf, it counts as unival tree

#ifdef __DEBUG__
	printf("Node 0x%p: is a leaf, returning 1\n", (void *)root);
#endif

		return 1l;
	}

	left_subtree = count_unival_trees(root->left_child, &is_valid);
	right_subtree = count_unival_trees(root->right_child, &is_valid);

#ifdef __DEBUG__
	if (is_valid)
		if ((root->left_child == NULL || equal_nodes_values(root, root->left_child))
			&& (root->right_child == NULL || equal_nodes_values(root, root->right_child)))
			printf("Node 0x%p: is a valid subtree\n", (void *)root);
		else
			printf("Node 0x%p: is NOT a valid subtree\n", (void *)root);
	else
		printf("Node 0x%p: is NOT a valid subtree because its children aren't\n", (void *)root);
#endif

	if (is_valid
			&& (root->left_child == NULL || equal_nodes_values(root, root->left_child))
			&& (root->right_child == NULL || equal_nodes_values(root, root->right_child)))
	{
		c = 1l;
	}
	else
	{
		if (return_validity != NULL)
		{
			*return_validity = 0l;
		}
	}

	return c + left_subtree + right_subtree;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	struct node *root, *tmp;
	unsigned long unival;

	// Build a test tree
	root = alloc_node((void *)0l, NULL, NULL);
	tmp = alloc_node((void *)1l, NULL, NULL);
	root->left_child = tmp;
	tmp = alloc_node((void *)0l, NULL, NULL);
	root->right_child = tmp;
	tmp = alloc_node((void *)1l, NULL, NULL);
	root->right_child->left_child = tmp;
	tmp = alloc_node((void *)0l, NULL, NULL);
	root->right_child->right_child = tmp;
	tmp = alloc_node((void *)1l, NULL, NULL);
	root->right_child->left_child->left_child = tmp;
	tmp = alloc_node((void *)1l, NULL, NULL);
	root->right_child->left_child->right_child = tmp;
	tmp = alloc_node((void *)0l, NULL, NULL);
	root->right_child->left_child->right_child->right_child = tmp;

	// Recursively count the unival subtrees
	unival = count_unival_trees(root, NULL);

	// Print the result
	printf("Unival trees: %lu\n", unival);

	// Free all the allocated memory
	destroy_tree(root);

	return 0;
}
