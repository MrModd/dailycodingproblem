#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "node.h"

struct node * alloc_node(void *value, struct node *left, struct node *right)
{
	struct node *tmp;

	tmp = malloc(sizeof(struct node));
	assert(tmp != NULL);

	tmp->value = value;
	tmp->left_child = left;
	tmp->right_child = right;

	return tmp;
}

void destroy_tree(struct node *root)
{
	if (root == NULL)
	{
		return;
	}

	destroy_tree(root->left_child);
	destroy_tree(root->right_child);

	free(root);
}
