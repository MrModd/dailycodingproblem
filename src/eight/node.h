#ifndef __NODE__
#define __NODE__

struct node {
	void *value;
	struct node *left_child;
	struct node *right_child;
};

/* struct node * alloc_node(void *value, struct node *left, struct node *right)
 *
 * Allocate the memory for a new node and assign to it the given
 * value, left child and right child (if any).
 *
 * The return value is a pointer to the allocated memory. */
struct node * alloc_node(void *, struct node *, struct node *);

/* Free all the nodes of a tree given its root node.
 *
 * NOTE: the value field of the nodes is not freed. It's up to
 * the caller to free (if necessary) that memory. */
void destroy_tree(struct node *);

#endif
