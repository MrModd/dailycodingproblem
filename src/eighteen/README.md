# Daily Coding Problem: Problem #18 [Hard]

Good morning! Here's your coding interview problem for today.

This problem was asked by Google.

Given an array of integers and a number `k`, where `1 <= k <= length
of the array`, compute the maximum values of each subarray of length `k`.

For example, given `array = [10, 5, 2, 7, 8, 7]` and `k = 3`, we should get:
`[10, 7, 8, 8]`, since:

```
    10 = max(10, 5, 2)
    7 = max(5, 2, 7)
    8 = max(2, 7, 8)
    8 = max(7, 8, 7)
```

Do this in `O(n)` time and `O(k)` space. You can modify the input array
in-place and you do not need to store the results. You can simply print
them out as you compute them.

## Solution

The solution consist in keeping a dequeue. This is a queue that can be
accessed on both sides. You can push front and push back. You can as well
remove from the front and from the back.

The content of the dequeue are the indexes of the useful element in the
input array.

The algorithm does 3 things: it processes the first `k` items, for each
index `k <= i <= n` it purges the indexes in the dequeue smaller than
`i - k` and then it evaluate the element at index `i` with the elements
at the indexes that are stored in the dequeue.

The process is the following:

- For each index `0 <= i < k` compare `array[i]` with the locations of
  `array` whose indexes are in the dequeue. Remove from the dequeue all
  the indexes for which the corresponding value in `array` is less than
  `array[i]`. At the end of each iteration add `i` in the dequeue.
- For each index `k <= i < n`:
  - Print the value in `array` pointed by the index you find at the
    head of dequeue
  - Remove from the back of the dequeue all the indexes that are previous
    to `i-k`, they are out of the current window
  - Remove indexes whose corresponding values in `array` are less than
   `array[i]`
  - Add `i` at the back of the dequeue
- Print the element of `array` whose index is at the head of the dequeue.

### Execution time

Execution is linear with the size of the array: `O(n)`.

### Memory space

Not considering the array required to store the input you need the
datastructure to keep the dequeue. This is `O(k)`.
