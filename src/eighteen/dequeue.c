#include <stdio.h>
#include <stdlib.h>
#include "dequeue.h"

void init(struct dequeue *q, int initial_size)
{
	q->queue = malloc(sizeof(int) * initial_size);
	if (q->queue == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer allocating q->queue\n");
		exit(1);
	}

	q->size = initial_size;
	q->elements = 0;
	q->first = 0;
}

void destroy(struct dequeue *q)
{
	free(q->queue);
}

int insert_front(struct dequeue *q, int i)
{
	int index;

	if (is_full(q))
	{
#ifdef __DEBUG__
		printf("insert_front: queue is full!\n");
#endif
		return 0; // Queue is full, can't add the new element
	}

	index = (q->first + q->elements++) % q->size;
	q->queue[index] = i;

#ifdef __DEBUG__
	printf("insert_front: q->first=%d q->elements=%d q->size=%d index=%d i=%d\n", q->first, q->elements, q->size, index, i);
#endif

	return 1;
}

int insert_last(struct dequeue *q, int i)
{
	int index;

	if (is_full(q))
	{
#ifdef __DEBUG__
		printf("insert_last: queue is full!\n");
#endif
		return 0;
	}

	if (!is_empty(q))
	{
		q->first = (q->first - 1 + q->size) % q->size;
	}

	index = q->first;
	q->elements++;
	q->queue[index] = i;

#ifdef __DEBUG__
	printf("insert_last: q->first=%d q->elements=%d q->size=%d index=%d i=%d\n", q->first, q->elements, q->size, index, i);
#endif

	return 1;
}

int delete_front(struct dequeue *q)
{
	if (is_empty(q))
	{
#ifdef __DEBUG__
		printf("delete_front: queue is empty!\n");
#endif
		return 0;
	}

	q->elements--;

#ifdef __DEBUG__
	printf("delete_front: q->first=%d q->elements=%d q->size=%d element=%d\n", q->first, q->elements, q->size, q->queue[(q->first + q->elements) % q->size]);
#endif

	return 1;
}

int delete_last(struct dequeue *q)
{
	if (is_empty(q))
	{
#ifdef __DEBUG__
		printf("delete_last: queue is empty!\n");
#endif
		return 0;
	}

	q->elements--;
	q->first = (q->first + 1) % q->size;

#ifdef __DEBUG__
	printf("delete_last: q->first=%d q->elements=%d q->size=%d element=%d\n", q->first, q->elements, q->size, q->queue[(q->first - 1 + q->size) % q->size]);
#endif

	return 1;
}

int get_front(struct dequeue *q)
{
	if (is_empty(q))
	{
		return 0;
	}

	return q->queue[(q->first + q->elements - 1) % q->size];
}

int get_rear(struct dequeue *q)
{
	if (is_empty(q))
	{
		return 0;
	}

	return q->queue[q->first];
}

int is_empty(struct dequeue *q)
{
	return (q->elements == 0);
}

int is_full(struct dequeue *q)
{
	return (q->elements == q->size);
}
