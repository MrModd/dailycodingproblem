#ifndef __DEQUEUE_H__
#define __DEQUEUE_H__

struct dequeue {
	int *queue;
	unsigned int size;
	unsigned int elements;
	unsigned int first;
};

void init(struct dequeue *, int);
void destroy(struct dequeue *);

int insert_front(struct dequeue *, int);
int insert_last(struct dequeue *, int);
int delete_front(struct dequeue *);
int delete_last(struct dequeue *);
int get_front(struct dequeue *);
int get_rear(struct dequeue *);

int is_empty(struct dequeue *);
int is_full(struct dequeue *);

#endif
