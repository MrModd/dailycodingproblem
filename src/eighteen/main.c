#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/strtol_error_handling.h"
#include "../common/utils.h"
#include "dequeue.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s k n1 n2 ...\n\n", name);
	fprintf(stderr, "k\t\tThe length of the subarrays\n");
	fprintf(stderr, "n1, n2, ...\tThe numbers of the array\n");
	fprintf(stderr, "RETURNS\n");
	fprintf(stderr, "0 as return code and the array of size k with the\n");
	fprintf(stderr, "max o the subarrays in the standard output.\n");
	fprintf(stderr, "A positive value in case of errors.\n");
}

int main(int argc, char **argv)
{
	// Input
	long k;
	long *input;
	unsigned int length;

	// Argument parse
	char *endptr;

	// Auxiliary structure
	struct dequeue q;
	unsigned int i;

	// Parse arguments

	if (argc < 3)
	{
		fprintf(stderr, "ERROR: You must specify at least the value k and an element of the array.\n");
		print_help(argv[0]);
		exit(1);
	}

	length = argc - 2;

	errno = 0;
	k = strtol(argv[1], &endptr, 10);
	STRTOL_ERROR_HANDLING(argv[1], endptr, 1);

	if (k > length)
	{
		fprintf(stderr, "ERROR: k must be <= the size of the array.\n");
		print_help(argv[0]);
		exit(1);
	}

	input = malloc(sizeof(long)*length);
	if (input == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer allocating \"input\"\n");
		exit(1);
	}

	init(&q, (int)k);

#ifdef __DEBUG__
	printf("Testing dequeue...\n");
	i = 0;
	while(!is_full(&q))
	{
		insert_front(&q, i++);
	}
	while (!is_empty(&q))
	{
		delete_front(&q);
	}
	i = 0;
	while(!is_full(&q))
	{
		insert_last(&q, i++);
	}
	while (!is_empty(&q))
	{
		delete_last(&q);
	}
	printf("Testing done...\n");
#endif

	// Read the input array
	parse_array_long(input, argv + 2, length);

	// Algorithm
	// Process first k elements
	for (i = 0; i < k; ++i)
	{
		// Delete those indexes whose corresponding values in input are < input[i]
		while (!is_empty(&q) && input[i] >= input[get_rear(&q)])
		{
			delete_last(&q);
		}
		insert_last(&q, i);
	}

	// Process all others n-k elements
	for (; i < length; ++i)
	{
		// The index at the head of the dequeue corresponds to a max, print it
		printf("%ld ", input[get_front(&q)]);

		// Delete indexes smaller than i-k, they are out of the sliding window
		while (!is_empty(&q) && get_front(&q) <= i - k)
		{
			delete_front(&q);
		}

		// Delete those indexes whose corresponding values in input are < input[i]
		while (!is_empty(&q) && input[i] >= input[get_rear(&q)])
		{
			delete_last(&q);
		}

		insert_last(&q, i);
	}

	// Print the value at the index that is in the front of the queue, it's the last max
	printf("%ld\n", input[get_front(&q)]);

	destroy(&q);
	free(input);

	return 0;
}
