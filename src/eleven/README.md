# Daily Coding Problem: Problem #11 [Medium]

This problem was asked by Twitter.

Implement an autocomplete system. That is, given a query string `s` and a
set of all possible query strings, return all strings in the set that
have s as a prefix.

For example, given the query string `de` and the set of strings
`[dog, deer, deal]`, return `[deer, deal]`.

Hint: Try preprocessing the dictionary into a more efficient data
structure to speed up queries.

## Solution

An efficient data structure to store the dictionary is a prefix tree,
also known as Trie.

Each node contains a letter. Concatenating all the letters from the
root to a given node gives you a word or a prefix of a word.

All leaves are completed words, but intermediate nodes can be
complete words as well (eg. `dog` is a prefix for the word `dogsitter`,
but it's also a word itself). So a flag in the node must denote
if that given node is the end of a word or not.

### Execution time

Given `|V|` the cardinality of the nodes in the tree and `|E|`
the max cardinality of the childrens for a node (which is 26,
the letters in the alphabet) then:

- Inserting a word of length `l` takes `o(l*|E|)`. It is the cost
  of navigating the tree, either by creating new nodes if not
  present, either by discovering existing nodes.
- Finding a prefix of length `l` takes as well `o(l*|E|)`
  because it's the same kind of visit.
- Getting all the complete words starting from a prefix of
  length `l` requires navigating until the end of the
  prefix, again `o(l*|E|)` and considering the discovered
  node the new root it requires a Breadth First Search (BFS)
  which, at min is `o(1)` if the new root is a leaf and
  at max is `o(|V|+|E|)` is the prefix was the empty string.

### Memory space

`o(|V|)` with `|V|` the cardinality of the nodes in the tree.
