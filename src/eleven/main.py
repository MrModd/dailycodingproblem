#!/usr/bin/env python

import logging
from trie import Trie

def main():
    #logging.basicConfig(level=logging.WARNING)
    #logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    word_list = ['dog', 'house', 'people', 'doctor', 'hotel', 'palm', 'day', 'dogsitter']
    tree = Trie(word_list)

    logging.debug('\n\n')

    print('Prefix \'a\': ' + str(tree.findPrefix('a')))
    print('Prefix \'hotel\': ' + str(tree.findPrefix('hotel')))
    print('Prefix \'d\': ' + str(tree.findPrefix('d')))
    print('Prefix \'do\': ' + str(tree.findPrefix('do')))
    print('Prefix \'dog\': ' + str(tree.findPrefix('dog')))
    print('Prefix \'\': ' + str(tree.findPrefix('')))

if __name__ == "__main__":
    main()
