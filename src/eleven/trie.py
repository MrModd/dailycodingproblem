import logging

logger = logging.getLogger(__name__)

class Node:
    """This is a node of a prefix tree.
       If the attribute complete_word is set to True then the Node is the
       end of a word. Otherwise it's only an intermediate letter of a complete word.
       The value of a node is only a letter. The word is built concatenating all the
       letters while navigating the tree.
    """

    def __init__(self, this_value=''):
        self.value = this_value
        self.children = []
        self.complete_word = False

    def __str__(self):
        return '<Node: ' + hex(id(self)) + ', value=\"' + self.value + '\">'

    def __repr__(self):
        return self.__str__()

    def getValue(self):
        return self.value

    def addChild(self, child):
        self.children.append(child)

    def getChildren(self):
        return self.children

    def isComplete(self):
        return self.complete_word

    def setComplete(self, isComplete):
        self.complete_word = isComplete

class Trie:
    """This is a prefix tree (or trie).
       It takes words (or strings generally speaking) as input
       and it decomposes them so that each node in the tree is
       a letter of the word.
       Words that has a prefix in common shares the first part of
       the visit of the tree. From the root until the last letter in
       common.
    """

    def __init__(self, this_word_list = []):
        self.root = Node()
        self.addWordList(this_word_list)

    def findChild(self, children, char_to_find):
        for child in children:
            logger.debug('       Checking child ' + str(child))
            if child.getValue() == char_to_find:
                return child
        return None

    def addWord(self, word: str):
        logger.info('### Adding word \"' + word + '\" ###')
        current_node = self.root
        logger.debug('  Starting node: ' + str(current_node))

        for char in word:
            found_child = None
            logger.debug('    Looking for \"' + str(char) + '\"')
            logger.debug('    current_node ' + str(current_node))
            logger.debug('    children of ' + str(current_node) + ': ' + str(current_node.getChildren()))

            found_child = self.findChild(current_node.getChildren(), char)

            if found_child == None:
                tmp = Node(char)
                current_node.addChild(tmp)
                current_node = tmp
                logger.debug('       * New node ' + str(tmp))
            else:
                current_node = found_child
                logger.debug('       * Existing node ' + str(current_node))

        current_node.setComplete(True)

    def addWordList(self, this_word_list):
        for word in this_word_list:
            self.addWord(word)

    def findPrefix(self, prefix):
        logger.info('### Finding prefix \"' + prefix + '\" ###')
        current_node = self.root
        words = []
        logger.debug('  Starting node: ' + str(current_node))

        for char in prefix:
            found_child = None
            logger.debug('    Looking for \"' + str(char) + '\"')
            logger.debug('    current_node ' + str(current_node))
            logger.debug('    children of ' + str(current_node) + ': ' + str(current_node.getChildren()))

            found_child = self.findChild(current_node.getChildren(), char)

            if found_child == None:
                logger.debug('       * Nothing found')
                return words
            else:
                current_node = found_child
                logger.debug('       * Existing node ' + str(current_node))

        # BFS
        logger.debug('  Beginning a BFS from ' + str(current_node))
        queue = [(prefix, current_node)]
        while len(queue) > 0:
            current_prefix, current_node = queue.pop(0)
            logger.debug('    Visiting ' + str(current_node))
            if current_node.isComplete():
                logger.debug('    * This node is a complete word. Adding word \"' + current_prefix + '\"')
                words.append(current_prefix)
            for child in current_node.getChildren():
                queue.append( (current_prefix + child.getValue(), child) )

        return words
