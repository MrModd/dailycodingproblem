# Daily Coding Problem: Problem #15 [Medium]

This problem was asked by Facebook.

Given a stream of elements too large to store in memory, pick a
random element from the stream with uniform probability.

## Solution

One problem here is that you cannot know the number of elements
that will arrive because it's a stream.

The solution is to use an exponential distribution.
First elements on the stream have more probability to be
chosen, but once they are discaded in favour of newer ones
they cannot come back.

Given a function `store_next(p)` which returns `1` with
probability `p`, you decide to store the `i-th` element
if `store_next(1/i) = 1`. Each time you decide to store,
you update the variable containing the only element you
are allowed to store.

### Execution time

The execution complexity is `o(n)`, you only need to
scan the input stream.

### Memory space

Memory space is constant: `o(1)`. You only store one
element plus some technical variables.
