#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define BUFFER_SIZE 64

unsigned int store_next(double p)
{
	return (rand() / (double)RAND_MAX <= p) ? 1 : 0;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	char buffer[BUFFER_SIZE], stored_value[BUFFER_SIZE];
	unsigned int seed = time(NULL);
	unsigned int total = 0;

	// Init rand()
	srand(seed);

	printf("Type the messages (max %d bytes per message) separated by a new line.\n", BUFFER_SIZE);
	printf("Terminate the reading with CTRL+D.\n");

	while(fgets(buffer, BUFFER_SIZE, stdin) != NULL)
	{
		if (store_next(1.0/++total))
		{
#ifdef __DEBUG__
			printf("Picked %s", buffer);
#endif
			strcpy(stored_value, buffer);
		}
	}

	printf("Program terminated.\n");
	printf("Randomly picked element: %s", stored_value);
#ifdef __DEBUG__
	printf("Total messages: %u.\n", total);
#endif

	return 0;
}
