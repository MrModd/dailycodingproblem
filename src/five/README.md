# Daily Coding Problem: Problem #5 [Medium]

This problem was asked by Jane Street.

`cons(a, b)` constructs a pair, and `car(pair)` and `cdr(pair)` returns the
first and last element of that pair. For example, `car(cons(3, 4))`
returns `3`, and `cdr(cons(3, 4))` returns `4`.

Given this implementation of cons:

```
def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair
```

Implement `car` and `cdr`.

## Solution

The key to this problem is to understand the `cons()` function.
This function is an example of **closure**. A closue is a function
that remembers some values in his scope, but there's no need to
specify them as arguments to the function call.

To make a step back the closure is based on the concept of
*inner function*. An inner function (also anonymous function) is
simply a function that has been nested inside another function.
An example is the following:

```
def outer_function():
    def inner_function(x):
        print(x)
    inner_function("Hello")
```

A closure adds varibles that are kept within the definition itself
of the inner function. For example:

```
def multiply(x):
    def inner(y):
        return x*y
    return inner

times5 = multiply(5)
print(times5(10))
```

This will print `50`. The variable `times5` contains the reference to
the function `inner()`. Since the reference has been created by the
call `multiply(5)` it will always keep the value `x=5` in its definition.

To come back to the problem, `cons()` returns the inner function `pair()`
and it preserves in its definition the values `a` and `b`.
The function `pair()` takes one argument `f`. `f` must be actually a function
that takes two arguments and that returns one of the two arguments.
Our final goal is to provide two implementations of the function `f()`, one
will return the implicit value `a` and one will return `b`.

### Execution time

`o(1)` since the program has a constant number of steps that don't depend
on the input.

### Memory space

`o(1)`: there's only the memory declared for the function definitions.
