# The function cons() takes 2 values in inputs and
# returns a function definition in output.
def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair

def car(f):
    '''
    This is how car() is used:
        ret_cons = cons(3,4)
        car(ret_cons)
    Where ret_cons is pair(f), the inner
    function of cons().
    pair(f) takes a function in input, so we have
    to define it. Here is the f2(a,b) function.
    '''
    def f2(a, b):
        return a # This function returns the first element of the couple
    '''
    Finally we make the call to pair(f). We pass f2() we just defined.
    The call to this function happens inside the inner function pair().
    '''
    return f(f2)

def cdr(f):
    def f2(a, b):
        return b
    return f(f2)
