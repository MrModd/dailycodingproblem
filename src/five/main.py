#!/usr/bin/env python

from functions import cons, car, cdr

def main():
    print(car(cons(3, 4)))
    print(cdr(cons(3, 4)))

if __name__ == "__main__":
    main()
