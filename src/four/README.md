# Daily Coding Problem: Problem #4 [Hard]

This problem was asked by Stripe.

Given an array of integers, find the first missing positive integer
in linear time and constant space. In other words, find the lowest
positive integer that does not exist in the array. The array can
contain duplicates and negative numbers as well.

For example, the input `[3, 4, -1, 1]` should give `2`.
The input `[1, 2, 0]` should give `3`.

You can modify the input array in-place.

## Solution

First step is to get rid of the negative numbers and zeroes. By reusing
the same array we can simply move them at the beginning.

Once the non positives numbers rely on the first part of the array we can
ignore them and start considering the array starting from the first
positive value.

For each value `a[i]` (say `j`) we mark the location `j-1` by flipping the
sign of the value `a[j-1]`. It's important to note that if `a[j-1]` is
already marked (thus it's already a negative value) we don't have to
make it positive in case we encounter it a second time.

if `a[i]` is greater than the size of the array just ignore it.
The reason is that if there's a number outside the size of the array
it means there must be a number missing in the interval `[0, size)`.
This means that `a[i]` is not the first missing positive.

### Execution time

`o(4n)=o(n)`: One first pass to build the array, one second pass to
move the negative values, one third pass to mark the present numbers
and finally one pass to find the first missing number.

### Memory space

`o(1)`: if you exclude the array that contains the input (otherwise
`o(n)`)
