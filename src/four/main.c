#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/strtol_error_handling.h"
#include "../common/utils.h"

#define ERROR 1

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s n1 n2 n3 ...\n\n", name);
	fprintf(stderr, "n1,n2,...\tthe numbers in the array\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "%d in case there was an error.\n", ERROR);
	fprintf(stderr, "0 otherwise and the first missing integer\nvalue in the stdout.\n");
}

/* Swap the content of the pointers *a and *b */
inline void swap_long(long *a, long *b)
{
	long tmp = *a;
	*a = *b;
	*b = tmp;
}

/* Move all the non positives numbers (<=0) to the beginning
 * of the array *numbers and return the first element
 * in the array with a positive numbers if any, or size otherwise */
int push_non_positives_back(long *numbers, int size)
{
	int i, j=0;

	for (i = 0; i < size; ++i)
	{
		if (numbers[i] <= 0) {
			swap_long(numbers+i, numbers+j);
			j++;
		}
	}

#ifdef __DEBUG__
	print_array(stderr, "Rearranged array", numbers, size);
#endif

	return j;
}

/* Given an array of positives numbers flip the sign of
 * the value in location numbers[i]-1 if it is below the
 * size of the array */
void flip_existing(long *numbers, int size)
{
	int i;

	for ( i = 0; i < size; ++i)
	{
		long n = labs(numbers[i]);
		if (n <= size)
		{
			long j = n-1;
			numbers[j] = labs(numbers[j])*-1;
		}
	}

#ifdef __DEBUG__
	print_array(stderr, "Flipped array (excluded initial negatives)", numbers, size);
#endif
}

int main(int argc, char **argv)
{
	int size = argc-1, offset, i;
	long *input;

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify at least one element of the input array.\n");
		print_help(argv[0]);
		exit(ERROR);
	}

	input = malloc(sizeof(long)*size);
	if (input == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(ERROR);
	}

	parse_array_long(input, argv+1, size);

	offset = push_non_positives_back(input, size);

	flip_existing(input+offset, size-offset);

	for (i = 0; i < size-offset; ++i)
	{
		if (input[i+offset] > 0)
		{
			printf("%d\n", i+1);
			free(input);
			exit(0);
		}
	}

	/* There's no positive number in the array.
	 * This means all the numbers were consecutive. The first
	 * missing positive number must be the first number outside
	 * the array */
	printf("%d\n", i+1);

	free(input);

	return 0;
}
