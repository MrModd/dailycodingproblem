# Daily Coding Problem: Problem #14 [Medium]

This problem was asked by Google.

The area of a circle is defined as πr^2. Estimate π to 3 decimal places
using a Monte Carlo method.

Hint: The basic equation of a circle is x^2 + y^2 = r^2.

## Solution

If you draw a circle of radius 1 on a cartesian plain you get a quarter
of circle in the positive part of the plain (x > 0 and y > 0).

The probability that picking random point in the interval `[(0,0),(1,1)]`
those points fall inside the circumference is directly related to pi.
In fact the the number of points inside the circumference divided by
the number of total drawn points tends to `4*pi`.

So for a very large number of samples, if `in_circle` is the number of
points that are within the circumference and `total` is the total number
of tests done, then:

`pi ~ 4 * in_circle / total`

To make the solution a bit more challenging I used OpenMP to parallelize
the program. By default the number of threads is equal to the number of
CPUs, but you can change it by setting the env variable `OMP_NUM_THREADS`.

For example: `OMP_NUM_THREADS=8 ./main`.

If you compile the program with `make debug` you'll get the number of
samples computed in a second.

### Execution time

The program doesn't really end until you kill it...
Anyway the complexity of the problem is `o(n)`, where `n` is the number
of samples.

### Memory space

The memory space is constant: `o(1)`. Only 2 variables are needed (plus
some technical ones).
