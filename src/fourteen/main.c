#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>

#define BLOCK 10000000

// Using the padding to avoid the ping pong on the line
// of cache.
// For Intel CPU a line of cache should be 64bytes long.
struct padded_uint {
	unsigned int n;
	char padding[64 - sizeof(unsigned int)];
};

/* Using rand_r to be thread safe.
 * srand is the state of the random generator. */
double next_rand(unsigned int *srand)
{
	return rand_r(srand) / (double)RAND_MAX;
}

/* Evaluate n samples using Monte Carlo approach to
 * approximate pi.
 * It returns the number of samples that fell in
 * the circle. */
unsigned long long compute_batch(unsigned long n, unsigned int *srand)
{
	unsigned long i;
	unsigned long long in_circle = 0;
	double x, y;

	for (i = 0; i < n; ++i)
	{
		x = next_rand(srand);
		y = next_rand(srand);

		if ((x*x) + (y*y) <= 1)
		{
			++in_circle;
		}
	}

	return in_circle;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	struct padded_uint *rand_seeds; // Each thread has its own random seed
	unsigned long long in_circle = 0, total = 0;
#ifdef __DEBUG__
	struct timeval tval_before, tval_after, tval_elapsed;
	double elapsed;
#endif

	// Allocate the memory for the seeds
	rand_seeds = malloc(sizeof(struct padded_uint)*omp_get_max_threads());
	if (rand_seeds == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer allocating rand_seed.\n");
		exit(1);
	}

	// Initialize the seeds for each thread
	// (each threads initializes its own)
#pragma omp parallel
	{
		rand_seeds[omp_get_thread_num()].n = time(NULL) + omp_get_thread_num();

#ifdef __DEBUG__
#pragma omp single
		{
			printf("Number of threads: %d.\n", omp_get_max_threads());
			fflush(stdout);
		}
#pragma omp critical
		{
			printf("Thread %d -> seed=%d.\n", omp_get_thread_num(), rand_seeds[omp_get_thread_num()].n);
			fflush(stdout);
		}
#endif
	}

	// Interrupt the program with CTRL+C
#pragma omp parallel
	while(1)
	{
		unsigned long long tmp;

#ifdef __DEBUG__
#pragma omp single
		gettimeofday(&tval_before, NULL);
#endif

		tmp = compute_batch(BLOCK, &(rand_seeds[omp_get_thread_num()].n));
#pragma omp atomic
		in_circle += tmp;
#pragma omp atomic
		total += BLOCK;

#pragma omp single
		{
#ifdef __DEBUG__
			gettimeofday(&tval_after, NULL);
			timersub(&tval_after, &tval_before, &tval_elapsed);
			elapsed = tval_elapsed.tv_sec + ((double)tval_elapsed.tv_usec / 1000000);
			printf("Samples/s: %e, ", BLOCK * omp_get_max_threads() / elapsed);
#endif

			printf("Samples: %llu, ", total);
			printf("Current estimation of pi: %.10f\r", (double)in_circle*4/total);
		}
	}

	free(rand_seeds);
	return 0;
}
