# Daily Coding Problem: Problem #9 [Hard]

This problem was asked by Airbnb.

Given a list of integers, write a function that returns the largest sum of
non-adjacent numbers. Numbers can be `0` or negative.

For example, `[2, 4, 6, 2, 5]` should return `13`, since we pick `2`, `6`,
and `5`. `[5, 1, 1, 5]` should return `10`, since we pick `5` and `5`.

Follow-up: Can you do this in O(N) time and constant space?

## Solution

The optimal solution requires only to maintain 2 accumulators during the
scan of the array.

At the generic step `i` the variable `max_including` has the maximum sum
of non-adjacent numbers in the range `[0,i-1]` and `i-1` is part of
the sum. The variable `max_excluding` as well has the maximum sum
of non-adjacent numbers in the range `[0,i-1]`, but this time `i-1`
is excluded from the sum.

To build the step `i` `max_including` is updated taking `max_excluding`
from the previous step and adding the number of the array at index `i`.
You can do this because `max_excluding` doesn't contain the number at
index `i-1` by definition, so adding the current index holds the
requirement of summing only non-adjacent numbers. The variable
`max_excluding` is updated and becomes the maximum between `max_including`
and `max_excluding` of the step `i-1`. We are not summing up the current
number, so at step `i` `max_excluding` is again excluding the number
at index `i`.

### Execution time

`o(n)` with `n` the lenght of the array.

### Memory space

`o(1)` if we don't consider the input array. We only need 2 scalars.
