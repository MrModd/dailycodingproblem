#include <stdio.h>
#include <stdlib.h>
#include "../common/utils.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s n1 n2 n3 ...\n\n", name);
	fprintf(stderr, "n1,n2,...\tthe numbers in the array\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "0 and the max sum in stdout.\n");
	fprintf(stderr, "a positive vaue in case there was an error.\n");
}

long compute_max_sum(long *numbers, unsigned int size)
{
	long max_including = 0, max_excluding = 0;
	unsigned int i;

	for (i = 0; i < size; ++i)
	{
#ifdef __DEBUG__
		printf("Step %u: max_including_prev=%ld, max_excluding_prev=%ld, current number %ld.\n", i, max_including, max_excluding, numbers[i]);
#endif
		long next_max_excl = (max_excluding > max_including) ? max_excluding : max_including;
		max_including = max_excluding + numbers[i];
		max_excluding = next_max_excl;
	}

	return (max_excluding > max_including) ? max_excluding : max_including;
}

int main(int argc, char **argv)
{
	long *numbers, max;

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify at least one element of the input array.\n");
		print_help(argv[0]);
		exit(1);
	}

	numbers = malloc(sizeof(long)*(argc-1));
	if (numbers == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(1);
	}

	parse_array_long(numbers, argv+1, argc-1);

	max = compute_max_sum(numbers, argc-1);

	printf("Max sum of non-adjacent numbers: %ld.\n", max);

	return 0;
}
