# Daily Coding Problem: Problem #1 [Easy]

This problem was recently asked by Google.

Given a list of numbers and a number `k`, return whether any two numbers from the list add up to `k`.

For example, given `[10, 15, 3, 7]` and `k` of `17`, return true since `10 + 7` is `17`.

Bonus: Can you do this in one pass?

## Solution

Construct a sparse array `a` of `k+1` elements initialized to `0`.
For each given input `i` mark `a[i]` to `a[i]+1` (ignore inputs greater than `k`, they
will never sum up to `k`).

Check for each index `j` of the array `a` if the content is `>0`, if yes check as well
if the content of `a[k-j]` is `>0`.

If yes there's at least one couple of numbers in the input that sum to `k`.

**Special case**: if `a[j]>0` and `a[k-j]>0`, but also `k-j=j`, then you must verify
that `a[j]>1`.

### Execution time

`o(2n)=o(n)`: one pass to build the array and one second pass to find the couple.

### Memory space

`o(1)`: the size of the array only depends on the constant `k`, no matter how many
inputs are passed.
