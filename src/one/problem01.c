#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/strtol_error_handling.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s <k> [n1 n2 n3 ...]\n\n", name);
	fprintf(stderr, "k\t\tthe number to find\n");
	fprintf(stderr, "n1,n2,...\tthe numbers in the array\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "1 if k can be found as sum of 2 numbers of the array\n");
	fprintf(stderr, "0 if k cannot be made as sum of 2 numbers of the array.\n");
	fprintf(stderr, "99 in case there was an error.\n");
}

int main(int argc, char **argv)
{
	int i;
	long k, tmp, *numbers;
	char *endptr = 0;

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You have to specify at least the number to find.\n");
		print_help(argv[0]);
		exit(99);
	}

	errno = 0;
	k = strtol(argv[1], &endptr, 10);
	STRTOL_ERROR_HANDLING(argv[1], endptr, 99);

#ifdef __DEBUG__
	printf("k=%ld\n", k);
#endif

	if (k <= 0)
	{
		fprintf(stderr, "ERROR: k must be positive.\n");
		exit(99);
	}

	numbers = calloc(sizeof(long), k+1);
	if (numbers == NULL)
	{
		fprintf(stderr, "ERROR: calloc returned a NULL pointer.\n");
		exit(99);
	}

#ifdef __DEBUG__
	printf("Numbers: ");
#endif
	for(i = 2; i < argc; ++i) {
		errno = 0;
		tmp = strtol(argv[i], &endptr, 10);
		STRTOL_ERROR_HANDLING(argv[i], endptr, 99);
#ifdef __DEBUG__
		printf("%ld ", tmp);
#endif
		if (tmp < 0)
		{
			fprintf(stderr, "ERROR: %ld must be a positive number.\n", tmp);
			exit(99);
		}
		if (tmp <= k)
		{
			numbers[tmp]++;
		}
	}
#ifdef __DEBUG__
	printf("\n");
	printf("Couples:\n");
#endif

	for(i = 0; i <= k; ++i)
	{
#ifdef __DEBUG__
		printf("i=%d,k-i=%ld,numbers[i]=%ld,numbers[k-i]=%ld\n", i, k-i, numbers[i], numbers[k-i]);
#endif
		if (numbers[i] > 0 && ((k-i == i && numbers[i] > 1) || (k-i != i && numbers[k-i] > 0)))
		{
			printf("There's a couple of numbers that sums to k\n");
			exit(1);
		}
	}
	printf("There isn't any couple of numbers that sums to k\n");

	free(numbers);

	return 0;
}
