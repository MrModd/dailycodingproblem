#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include "../common/strtol_error_handling.h"
#include "charmap.h"

char decode_single(char digit)
{
	if (digit < '1' || digit > '9')
	{
		return '\0';
	}
	return digit - '1' + 'a';
}

char decode_double(char first_digit, char second_digit)
{
	long i;
	char str[3], *endptr;
	str[0] = first_digit;
	str[1] = second_digit;
	str[2] = '\0';

	errno = 0;
	i = strtol(str, &endptr, 10);
	STRTOL_ERROR_HANDLING(str, endptr, 1);

	if (i < 10 || i > 26)
	{
		return '\0';
	}

	return (char)i + 'a' - 1;
}
