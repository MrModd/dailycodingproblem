#ifndef __CHARMAP__
#define __CHARMAP__

char decode_single(char digit);
char decode_double(char first_digit, char second_digit);

#endif
