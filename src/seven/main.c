#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "node.h"
#include "charmap.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s <string>\n\n", name);
	fprintf(stderr, "Where <string> is an encoded set of character mapped in\n");
	fprintf(stderr, "the following way:\n");
	fprintf(stderr, "1=a\n2=b\n3=c\n...\n26=z\n\n");
	fprintf(stderr, "Example of input string: 111 or 1263.\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "0 in case of success and the number of valid decoded words in stdout.\n");
	fprintf(stderr, "not 0 in case there was an error.\n");
}

/* Append a char to a \0 terminated string */
void append_char(char *dst, char c)
{
	unsigned int i = 0;

	while (dst[i] != '\0')
	{
		++i;
	}

	dst[i] = c;
	dst[i+1] = '\0';
}

int main(int argc, char **argv)
{
	char *input_string;
	size_t input_size;
	struct node *queue = NULL;
	size_t queue_size = 0;
	unsigned long i, visiting, valid;

	// Parse arguments
	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify the input string.\n");
		print_help(argv[0]);
		exit(1);
	}

	input_string = argv[1];
	input_size = strlen(input_string);

#ifdef __DEBUG__
	printf("Size of input string: %lu\n", input_size);
#endif

	// Allocate the first element of the queue
	queue_size = add(&queue, queue_size, input_size);

	// Algorithm
	// Consider the first element of the queue as an element present in the queue
	visiting = 0;
	valid = 1; // Count the first (and only) element in the queue as a valid word
	while (visiting < queue_size) // Until the queue is all visited
	{
#ifdef __DEBUG__
		fprintf(stderr, "==reading node %lu\n", visiting);
#endif
		while (queue[visiting].start_index < input_size)
		{
			// Get next input digit and decode it into a letter
			char next_digit = input_string[queue[visiting].start_index];
			char next_char = decode_single(next_digit);

			// If there's another digit after attempt to parse the two together
			if (queue[visiting].start_index+1 < input_size) {
				char next_next_digit = input_string[queue[visiting].start_index+1];
				// Decode the two digits together
				char next_next_char = decode_double(next_digit, next_next_digit);

				// If the two digits are in the range ["10", "26"]
				if (next_next_char != '\0')
				{
#ifdef __DEBUG__
					fprintf(stderr, "========pushing node %lu \n", queue_size);
#endif
					// Push this new substring in the queue for a future visit
					queue_size = add(&queue, queue_size, input_size); //Expand the queue size
					queue[queue_size-1].start_index = queue[visiting].start_index+2;
					strcpy(queue[queue_size-1].decoded_string, queue[visiting].decoded_string);
					append_char(queue[queue_size-1].decoded_string, next_next_char);
					++valid;
				}
			}

			// If the current char is the digit '0' it can't produce a valid letter.
			// For example the input "101" can only be interpreted as "ja".
			// For this example you start considering the first digit as the letter 'a',
			// but then you realize that the following digit cannot be converted.
			// At this point yo have to discard the string.
			if (next_char == '\0')
			{
				--valid;
#ifdef __DEBUG__
				queue[visiting].valid = 0;
#endif
				break; // Interrupt the parsing of the input for this word
			}
			else
			{
				++queue[visiting].start_index;
				append_char(queue[visiting].decoded_string, next_char);
			}
		}
		++visiting;
	}

#ifdef __DEBUG__
	printf("Valid words:\n");
	for (i = 0; i < queue_size; ++i)
	{
		if (queue[i].valid)
		{
			printf("%s\n", queue[i].decoded_string);
		}
	}
#endif

	// Print outcome
	printf("%lu\n", valid);

	// Free memory
	for (i = 0; i < queue_size; ++i)
	{
		free(queue[i].decoded_string);
	}
	free(queue);

	return 0;
}
