#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "node.h"

size_t add(struct node **queue, size_t queue_size, size_t string_size)
{
	*queue = realloc(*queue, sizeof(struct node)*(queue_size+1));
	assert(*queue != NULL);

	(*queue)[queue_size].start_index = 0;
#ifdef __DEBUG__
	(*queue)[queue_size].valid= 1;
#endif
	(*queue)[queue_size].decoded_string = calloc(sizeof(char), string_size+1);
	assert((*queue)[queue_size].decoded_string != NULL);

	return queue_size+1;
}
