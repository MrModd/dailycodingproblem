#ifndef __NODE__
#define __NODE__

struct node {
	unsigned int start_index;
#ifdef __DEBUG__
	unsigned int valid;
#endif
	char *decoded_string;
};

/* size_t add(struct node **queue, size_t queue_size, size_t string_size)
 *
 * Expand the given queue by allocating a new element for the array and
 * allocate the decoded_string field of the struct with a lenght of string_size.
 *
 * The return value is the new size of the queue (queue_size+1) */
size_t add(struct node **, size_t, size_t);

#endif
