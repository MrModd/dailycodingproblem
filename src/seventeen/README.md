# Daily Coding Problem: Problem #17 [Hard]

This problem was asked by Google.

Suppose we represent our file system by a string in the following manner:

The string `"dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"` represents:

```
dir
    subdir1
    subdir2
        file.ext
```

The directory `dir` contains an empty sub-directory `subdir1` and a
sub-directory `subdir2` containing a file `file.ext`.

The string `"dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"`
represents:

```
dir
    subdir1
        file1.ext
        subsubdir1
    subdir2
        subsubdir2
            file2.ext
```

The directory `dir` contains two sub-directories `subdir1` and `subdir2`. `subdir1`
contains a file `file1.ext` and an empty second-level sub-directory `subsubdir1`.
`subdir2` contains a second-level sub-directory `subsubdir2` containing a file `file2.ext`.

We are interested in finding the longest (number of characters) absolute path to
a file within our file system. For example, in the second example above, the
longest absolute path is `"dir/subdir2/subsubdir2/file2.ext"`, and its length is 32
(not including the double quotes).

Given a string representing the file system in the above format, return the length
of the longest absolute path to a file in the abstracted file system. If there is
no file in the system, return 0.

Note:

The name of a file contains at least a period and an extension.

The name of a directory or sub-directory will not contain a period.

## Solution

This is a parsing problem. The syntax reminds the one of Yaml language or even
Python, where indentation is used to express the blocks.

For this solution the objective is to keep one string which has the longest
path found up to now and one string which contains the full path of the
element in the input string that we are looking at a given moment.

On the generic step there are 2 possible moves:

- The new element from the input string is a child of the previous
  element. In this case we only append the new element to the
  string of the current under analysis path.
- The new element is at the same level or in a higher level  of the
  previous element. This means the previous path reached a dead end
  and we have to go back by one level in the folder tree. Before
  progressing with the next step we have to evaluate if the path in
  the previous step ended with a file and if yes if it was the
  longest one encountered so far.

### Execution time

The execution time is linked to the parsing of the input string.
There are some string copies and manipulations that could have been
optimized and they influence a bit the execution time.

In general we can say the program runs in `o(l)` with `l` the length
of the input string.

### Memory space

The memory usage could as well be optimized. The string buffers
containing the longest path and the current exploration are twice
the length of the input string for the limit case where the longest
path is made of folders of single characters. In this case the
worst case scenario is that for each letter you have to add a
folder divider `\`.

In a better implementation an array doubling would better fit the
solution.

To sum-up the memory usage is `o(l)` with `l` the length of the input
string.
