#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __DEBUG__
/* as printf(), but it prints from a start to an end pointers */
void printf_str(char *start, char *end)
{
	while (start < end)
	{
		printf("%c", *start);
		++start;
	}
}

/* Print the input string as a directory structure */
void print_tree(char *string)
{
	size_t len = strlen(string);
	unsigned int i;

	printf("Input tree:\n--------------------\n");
	for (i = 0; i < len; ++i)
	{
		if (string[i] == '\\' && i+1 < len && string[i+1] == 'n')
		{
			printf("\n");
			++i;
		}
		else if (string[i] == '\\' && i+1 < len && string[i+1] == 't')
		{
			printf("\t");
			++i;
		}
		else
		{
			printf("%c", string[i]);
		}
	}
	printf("\n--------------------\n\n");
}
#endif

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s <folder tree>\n\n", name);
	fprintf(stderr, "Where the folder tree is in the format\n");
	fprintf(stderr, "dir\\n\\tsubdir1\\n\\tsubdir2\\n\\t\\tfile.ext\n");
}

/* Parse the input string and returns in endstr the pointer
 * to the first character after the current file or foler name */
void get_next_word(char *str, char **endstr)
{
	size_t length = strlen(str);
	unsigned int i;
	*endstr = str;

	for (i = 0; i < length; ++i)
	{
		if (str[i] == '\\' && i+1 < length && str[i+1] == 'n')
		{
			break;
		}
		(*endstr)++;
	}

#ifdef __DEBUG__
	printf("    get_next_word(): ");
	printf_str(str, *endstr);
	printf("\n");
#endif
}

/* Parse the tabs between next file or dir and return the
 * pointer to the first char in the string after the
 * \t characters */
int get_next_tabs(char *str, char **next_word)
{
	size_t length = strlen(str);
	unsigned int i = 0, count = 0;

	if (length > 2 && str[0] == '\\' && str[1] == 'n')
	{
		// Ignore beginning new line
		i = 2;
	}

	for (; i+1 < length; i+=2)
	{
		if (str[i] == '\\' && str[i+1] == 't')
		{
			++count;
		}
		else
		{
			*next_word = str + i;
			break;
		}
	}

#ifdef __DEBUG__
	printf("    get_next_tab(): parsed ");
	printf_str(str, *next_word);
	printf(", next file or dir level is %u\n", count);
#endif

	return count;
}

int has_file(char *str, char *endstr)
{
#ifdef __DEBUG__
	printf("        has_file(): checking if ");
	printf_str(str, endstr);
	printf(" ends with a file --> ");
#endif

	--endstr;
	while (endstr > str)
	{
		if (*endstr == '.')
		{
#ifdef __DEBUG__
			printf("YES\n");
#endif
			return 1;
		}
		if (*endstr == '/')
		{
#ifdef __DEBUG__
			printf("NO\n");
#endif
			return 0;
		}
		endstr--;
	}
#ifdef __DEBUG__
	printf("NO\n");
#endif
	return 0;
}

/* Return the distance of the two pointers */
size_t strlen_ptr(char *startptr, char *endptr)
{
	return (endptr > startptr) ? endptr - startptr : 0;
}

/* Append the folder or file name within startptr and endptr in
 * bufer */
void append_path_element(char *buffer, char *startptr, char *endptr)
{
#ifdef __DEBUG__
	printf("    append_path_element(): before \"%s\" --> ", buffer);
#endif

	strcat(buffer, "/");
	strncat(buffer, startptr, strlen_ptr(startptr, endptr));

#ifdef __DEBUG__
	printf("after \"%s\"\n", buffer);
#endif
}

/* Remove "elements" number of folders from the path in buffer */
void remove_path_elements(char *buffer, unsigned int elements)
{
	size_t length = strlen(buffer);
	unsigned int i;

#ifdef __DEBUG__
	printf("    remove_path_elements(): removing %u elements, before \"%s\" --> ", elements, buffer);
#endif

	for (i = length-1; i > 0 && elements > 0; --i)
	{
		if (buffer[i] == '/')
		{
			--elements;
		}
		buffer[i] = '\0';
	}

#ifdef __DEBUG__
	printf("after \"%s\"\n", buffer);
#endif
}

/* Compare the length of the two input strings and if
 * current ends with a file and is longer than longest
 * then copy current over longests */
void compare_and_copy(char *longest, char *current)
{
	size_t current_len = strlen(current);

#ifdef __DEBUG__
	printf("    compare_and_copy(): longest=\"%s\" current=\"%s\"\n", longest, current);
#endif

	if (has_file(current, current + current_len) && current_len > strlen(longest))
	{
#ifdef __DEBUG__
		printf("    compare_and_copy(): current path is longer, copying over the previous longest path.\n");
#endif
		strcpy(longest, current);
	}
}

int main(int argc, char **argv)
{
	char *input, *end_input, *tmp_ptr;
	size_t input_len;

	char *longest_buffer, *current_buffer;
	unsigned int current_depth, next_depth;

	// Parse arguments

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify the folder tree.\n");
		print_help(argv[0]);
		exit(1);
	}

	input = argv[1];
	input_len = strlen(input);

	longest_buffer = calloc(sizeof(char), (input_len+1)*2);
	if (longest_buffer == NULL)
	{
		fprintf(stderr, "ERROR: calloc returned a NULL pointer allocating longest_buffer.\n");
		exit(1);
	}
	current_buffer = calloc(sizeof(char), (input_len+1)*2);
	if (current_buffer == NULL)
	{
		fprintf(stderr, "ERROR: calloc returned a NULL pointer allocating current_buffer.\n");
		exit(1);
	}

#ifdef __DEBUG__
	print_tree(input);
#endif

	// Init phase: get the root element
	current_depth = 0;
	end_input = input + input_len;
	get_next_word(input, &tmp_ptr);
	strncat(current_buffer, input, strlen_ptr(input, tmp_ptr));
	input = tmp_ptr;

#ifdef __DEBUG__
	printf("Root node is %s\nStarting iterations.\n", current_buffer);
#endif

	while(input < end_input)
	{
#ifdef __DEBUG__
		printf("We are at level %u.\n", current_depth);
#endif

		next_depth = get_next_tabs(input, &input);
		get_next_word(input, &tmp_ptr);
		if (input == tmp_ptr)
		{
			fprintf(stderr, "ERROR: malformed input string.\n");
			exit(1);
		}

		if (next_depth == current_depth + 1)
		{
			// Subdir or file
			append_path_element(current_buffer, input, tmp_ptr);
			++current_depth;
		}
		else if (next_depth <= current_depth)
		{
			// New subtree in case next_depth < current_depth
			// or file or folder in the same directory in case next_depth == current_depth
			compare_and_copy(longest_buffer, current_buffer);
			remove_path_elements(current_buffer, (current_depth - next_depth) + 1);
			append_path_element(current_buffer, input, tmp_ptr);
			current_depth = next_depth;
		}
		else
		{
			fprintf(stderr, "ERROR: malformed input string.\n");
			exit(1);
		}
		input = tmp_ptr;
	}

	compare_and_copy(longest_buffer, current_buffer);
	printf("Lenght of the longest path is %lu\n", strlen(longest_buffer));
#ifdef __DEBUG__
	printf("The path is: \"%s\"\n", longest_buffer);
#endif

	free(longest_buffer);
	free(current_buffer);

	return 0;
}
