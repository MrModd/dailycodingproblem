# Daily Coding Problem: Problem #6 [Hard]

This problem was asked by Google.

An XOR linked list is a more memory efficient doubly linked list.
Instead of each node holding `next` and `prev` fields, it holds a
field named `both`, which is an XOR of the next node and the previous
node. Implement an XOR linked list; it has an `add(element)` which
adds the element to the end, and a `get(index)` which returns the
node at index.

If using a language that has no pointers (such as Python), you can
assume you have access to `get_pointer` and `dereference_pointer`
functions that converts between nodes and memory addresses.

## Solution

This problem is not much different than creating a normal doubly
linked list.

This is the *truth table* of the XOR operation:

```
+---+---+---+
| A | B |OUT|
+---+---+---+
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |
+---+---+---+
```

What makes the XOR operation useful for this problem is that
you can store 2 values in the same variable and you can always
retrieve them if you have one of the two:

```
c = a ^ b
a = c ^ b
b = c ^ a
```

If you have two values, you can always retrieve the third one.

For this problem you can use the XOR to store both `prev` and
`next` as far as you keep at least a reference of one of the
two.

### Execution time

- `get()` operation: `o(n)`
- `add()` operation: `o(n)`
- `init_node()` operation: `o(1)`
- `next()` operation: `o(1)`
- `previous()` operation: `o(1)`

### Memory space

`o(n)`: each element of the list represent an data structure
in memory.
