#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "xor_linked_list.h"

int main(int argc, char **argv)
{
	(void)argc; // Avoid warnings for unused variables
	(void)argv;
	struct node *head, *tmp;
	unsigned long i;
	struct iterator j;

	init_node(&head);
	head->value = (void *)0l;

#ifdef __DEBUG__
	printf("head address: 0x%p\n", (void *)head);
#endif

	// Creating the list
	for (i = 1; i < 100; ++i)
	{
		init_node(&tmp);
		tmp->value = (void *)i;
		add(head, tmp);
	}

	// Getting each element of the list using get()
	printf("Testing the get() function...\n");
	printf("Elements in the list (printing first 10): ");
	for (i = 0; i < 10; ++i)
	{
		tmp = get(head, i);
#ifdef __DEBUG__
		printf("\nelement %lu, address: 0x%p, value", i, (void *)tmp);
		fflush(stdout);
#endif
		assert(tmp != NULL);
		printf(" %lu", (unsigned long)tmp->value);
	}
	printf("\n");

	// Getting each element starting from the head and using next() on an iterator
	printf("\nTesting the next function...\n");
	printf("Element in the list: ");
	init_iterator(j, head);
	tmp = head;
	do {
#ifdef __DEBUG__
		printf("\naddress: 0x%p, value", (void *)tmp);
		fflush(stdout);
#endif
		assert(tmp != NULL);
		printf(" %lu", (unsigned long)tmp->value);
	} while ((tmp = next(&j)) != NULL);
	printf("\n");

	// Getting each element starting from the last one and using previous() on an iterator
	printf("\nTesting the previous function...\n");
	printf("Element in the list (inverse order): ");
	while ((tmp = previous(&j)) != NULL)
	{
#ifdef __DEBUG__
		printf("\naddress: 0x%p, value", (void *)tmp);
		fflush(stdout);
#endif
		assert(tmp != NULL);
		printf(" %lu", (unsigned long)tmp->value);
	}
	printf("\n");

	return 0;
}
