#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "xor_linked_list.h"

void init_node(struct node **node)
{
	*node = malloc(sizeof(struct node));
	assert(*node != NULL);
	(*node)->prev_xor_next = NULL;
}

struct node * next(struct iterator *i)
{
	struct node *tmp;

	if (i->curr == NULL)
	{
		return NULL;
	}

	tmp = i->prev;
	i->prev = i->curr;
	i->curr = (struct node *)((uintptr_t)i->curr->prev_xor_next ^ (uintptr_t)tmp);

	return i->curr;
}

struct node * previous(struct iterator * i)
{
	struct node *tmp;

	if (i->prev == NULL)
	{
		return NULL;
	}

	tmp = i->curr;
	i->curr = i->prev;
	i->prev = (struct node *)((uintptr_t)i->prev->prev_xor_next ^ (uintptr_t)tmp);

	return i->curr;
}


void add(struct node *head, struct node *new)
{
	struct node *prev = NULL, *curr = head;
	struct iterator i;
	init_iterator(i, head);

	while (curr != NULL)
	{
		prev = curr;
		curr = next(&i);
	}

	assert(prev != NULL);

	prev->prev_xor_next = (struct node *)((uintptr_t)prev->prev_xor_next ^ (uintptr_t)new);
	new->prev_xor_next = prev;
}

struct node * get(struct node *head, unsigned int index)
{
	struct node *curr = head;
	struct iterator i;
	init_iterator(i, head);
	unsigned int j;

	for (j = 0; j < index && curr != NULL; ++j)
	{
			curr = next(&i);
	}

	return curr;
}
