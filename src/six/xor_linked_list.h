#ifndef __XOR_LINKED_LIST__
#define __XOR_LINKED_LIST__

/* This is an element of the list.
 * You should keep a reference to the first
 * element of the list.*/
struct node {
	void *value;
	struct node *prev_xor_next;
};

/* Helper structure used to navigate
 * the list. */
struct iterator {
	struct node *prev;
	struct node *curr;
};

/* void init_iterator(struct iterator, struct node *);
 * Initialize an iterator making it point to the given
 * node. */
#define init_iterator(iterator, head) { \
		iterator.prev = NULL; \
		iterator.curr = head; \
}

/* Allocate the memory for a struct node *.
 * The pointer is returned as first argument
 * of the function call. */
void init_node(struct node **);

/* Add a node (must be already allocated) at the
 * end of the list.
 * First argument is the head of the list, the second
 * argument is the new node to add. */
void add(struct node *, struct node *);

/* Get the node at a given index of the list.
 * First argument is the head of the list, second
 * argument is the index.
 * The pointer to the node is returned unless the
 * head was NULL or the index is outside the range. */
struct node * get(struct node *, unsigned int);

/* Move the iterator to the next node of the list
 * and return it. */
struct node * next(struct iterator *);

/* Move the iterator to the previous node of the
 * list and return it. */
struct node * previous(struct iterator *);

#endif
