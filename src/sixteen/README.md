# Daily Coding Problem: Problem #16 [Easy]

This problem was asked by Twitter.

You run an e-commerce website and want to record the last `N order`
ids in a log. Implement a data structure to accomplish this,
with the following API:

- record(order_id): adds the order_id to the log
- get_last(i): gets the ith last element from the log. i is
  guaranteed to be smaller than or equal to N.

You should be as efficient with time and space as possible.

## Solution

A possible solution is to use a circular array that overwrites
older entries when it gets full.

A circular array is a data structure made of 4 variables:

- `array`: the array containig the values
- `size`: the size of the array
- `begin`: the index of the first element in the array
- `elements`: how many elements are present in the array

Adding a new element means to fill the index
`(beginning + elements) % size` and increase by `1` the
variable `elements`.

If the array is full (`elements == size`) then `elements`
is not increased, but `beginning` does so that the old
value becomes the news entry in the array.

### Execution time

- `record(order_id)`: adding a new element is done in
  constant time, it's a sum and a module on the indexes
  of the data structure -> `o(1)`.
- `get_last(i)`: getting element `i` from the array is
  done in constant time. The array index is directly found
  using sums and modules -> `o(1)`.

### Memory space

The circular array requires `o(n)` space to store `n`
elements. This includes the array of length `n` plus
3 variables of fixed length.
