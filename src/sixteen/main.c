#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include "../common/strtol_error_handling.h"

#define BUFFER_SIZE 64

struct circular_array {
	long *array;               // The circular array containing the chars
	unsigned int size;         // The total size of the array
	unsigned int begin;        // The index of the first valid element
	unsigned int elements;     // The number of valid elements
};

/* Add a new value in the cyclic array.
 * If the array is full it overrides the oldest entry */
void record(struct circular_array *a, long c)
{
	a->array[(a->begin + a->elements) % a->size] = c;
	if (a->elements < a->size)
	{
		a->elements++;
	}
	else
	{
		a->begin = (a->begin + 1) % a->size;
	}
}

/* Get the i-th element of the circular array a */
long get_last(struct circular_array a, unsigned int i)
{
	assert(i < a.elements || i > a.size);
	return a.array[(a.begin + i) % a.size];
}

/* Remove the trailing new line in the string s */
void sanitize_string(char *s)
{
	size_t l = strlen(s);
	if (l > 0 && s[l-1] == '\n')
	{
		s[l-1] = '\0';
	}
}

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s N\n\n", name);
	fprintf(stderr, "N\tthe number of most recent ids to store.\n");
}

int main(int argc, char **argv)
{
	long N, last_id;
	unsigned int i;
	char *endptr = NULL, buffer[BUFFER_SIZE];
	struct circular_array last_ids;

	// Parse arguments

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify N.\n");
		print_help(argv[0]);
		exit(1);
	}

	errno = 0;
	N = strtol(argv[1], &endptr, 10);
	STRTOL_ERROR_HANDLING(argv[1], endptr, 1);

	// Init the data structures

	last_ids.array = malloc(sizeof(long)*N);
	if (last_ids.array == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer allocating last_ids.array.\n");
		exit(1);
	}
	last_ids.size = N;
	last_ids.begin = 0;
	last_ids.elements = 0;

	// Beginning of the program
	
	printf("Type the IDs (max %d bytes per input) separated by a new line.\n", BUFFER_SIZE);
	printf("Terminate the reading with CTRL+D.\n");

	while(fgets(buffer, BUFFER_SIZE, stdin) != NULL)
	{
		sanitize_string(buffer);

		errno = 0;
		last_id = strtol(buffer, &endptr, 10);
		if (buffer == endptr || !(buffer[0] != '\0' && *endptr == '\0') || errno != 0)
		{
			fprintf(stderr, "WARN: converting string \"%s\" into long. Ignoring value.\n", buffer);
		}
		else
		{
			record(&last_ids, last_id);
		}
	}

	printf("Reading terminated.\n");
	printf("These are the last %ld recorded IDs:\n", N);
	for (i = 0; i < last_ids.elements; ++i)
	{
		printf("%ld ", get_last(last_ids, i));
	}
	printf("\n");

	free(last_ids.array);

	return 0;
}
