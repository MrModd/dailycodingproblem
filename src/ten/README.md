# Daily Coding Problem: Problem #10 [Medium]

This problem was asked by Apple.

Implement a job scheduler which takes in a function `f` and an
integer `n`, and calls `f` after `n` milliseconds.

## Solution

You just need to declare a function that takes a function as
argument.

In C a function pointer is declared like this:
`int (*function)(int)`

This takes the pointer of a funtion with the signature
`int function(int);`

If you ever used pthreads in C you should easily recognize
this syntax.

### Execution time

*Not applicable*

The execution time depends on the called function.

### Memory space

`o(1)` as we don't need to allocate anything special.
