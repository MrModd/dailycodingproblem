#include <stdio.h>
#include <stdlib.h>
#include "scheduler.h"

void print_stuff(void *args)
{
	(void)args; // Not using the argument
	printf("Hello from function print_stuff().\n");
}

void print_string(void *args)
{
	char *string = (char *)args;

	printf("print_string(): %s\n", string);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	char *string_to_print = "I'm an argument!!!";

	if (schedule_millis(print_stuff, NULL, 2588) != 0)
	{
		printf("The job didn't run due to an error in nanosleep().\n");
	}

	if (schedule_millis(print_string, (void *)string_to_print, 54390) != 0)
	{
		printf("The job didn't run due to an error in nanosleep().\n");
	}

	return 0;
}
