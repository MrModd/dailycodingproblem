#include <stdio.h>
#include <time.h>
#include <errno.h>
#include "scheduler.h"

int schedule_millis(void (*job)(void *), void *args, unsigned long long milliseconds)
{
#ifdef __DEBUG__
	printf("===> Entering schedule_millis.\n");
#endif

	struct timespec t;
	int ret;

	// Populate the timestructure used by nanosleep()
	t.tv_sec = milliseconds / 1000;
	t.tv_nsec = (milliseconds % 1000) * 1000;

#ifdef __DEBUG__
	printf("===> Sleeping for %llu ms.\n", milliseconds);
#endif

	// Sleep and check if it gets interrupted
	errno = 0;
	ret = nanosleep(&t, NULL);
	if (ret != 0)
	{
		perror("nanosleep");
		return ret;
	}

#ifdef __DEBUG__
	printf("===> Scheduling the job.\n");
#endif

	// Call the function
	job(args);

#ifdef __DEBUG__
	printf("===> Job done.\n");
#endif

	return 0;
}
