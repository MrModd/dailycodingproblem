#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

/* Run a job after n milliseconds */
int schedule_millis(void(*job)(void *), void *, unsigned long long);

#endif
