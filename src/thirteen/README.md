# Daily Coding Problem: Problem #13 [Hard]

This problem was asked by Amazon.

Given an integer k and a string s, find the length of the longest
substring that contains at most k distinct characters.

For example, given s = "abcba" and k = 2, the longest substring with
k distinct characters is "bcb".

## Solution

To solve the problem it's sufficient to keep two buckets: one
for the letters that are part of the current substring and one
for the number of occurrences that each letter in the bucket
has in the current substring. The size of the two buckets
is `k`.

For each letter of the input string we check if it is already
present in the bucket. These are the possible cases:

- The letter is not present and the bucket of letters is not full:
  we simply add the letter in the bucket and we count 1 occurrence
  for it in the other bucket;
- The letter is not present and the bucket of letters is full:
  we sum up all the occurrences in the buckets to see how long is
  the current substring. If this substring is the longest one
  until now we update a variable that contains the size of the
  longest substring. In either cases we replace the oldest letter
  in the bucket of letters with the new one and we reset the bucket
  of occurrences for this letter to 1;
- The letter is present: we update the counter for this letter in
  the bucket of occurrences.

At the end we compare again the sum of all the occurrences in the
bucket and the variable with the lenght of the longest substring.
We display the biggest between the two.

The buckets can be implemented as circular arrays.

### Execution time

`o(n*2k) = o(n*k)` where `n` is the length of the input string
and `k` is the number of distinct letters. The `2k` comes from
one first scan of the circular array to see if the current
letter is new and a second scan of the circular array to count
all the occurrences of the letters in the array.

### Memory space

`o(2k) = o(k)`: two circular arrays, one for the bucket of letters
and one for the bucket of occurrences.
