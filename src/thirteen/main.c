#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../common/strtol_error_handling.h"

struct circular_array {
	char *array;               // The circular array containing the chars
	unsigned int *occurrences; // The number of occurrences for each element
	unsigned int size;         // The total size of the array
	unsigned int begin;        // The index of the first valid element
	unsigned int elements;     // The number of valid elements
};

/* Add a new letter in the cyclic array.
 * If the array is full it overrides the oldest entry */
void add_new(struct circular_array *a, char c)
{
	a->array[(a->begin + a->elements) % a->size] = c;
	a->occurrences[(a->begin + a->elements) % a->size] = 1;
	if (a->elements < a->size)
	{
		a->elements++;
	}
	else
	{
		a->begin = (a->begin + 1) % a->size;
	}
}

/* If the letter c is new it returns 1.
 * If the letter c is already present in the array
 * it adds 1 to the number of occurrences and it returns 0. */
int check_new_or_count(struct circular_array a, char c)
{
	unsigned int i;

	for (i = 0; i < a.elements; ++i)
	{
		if (a.array[(a.begin + i) % a.size] == c)
		{
			a.occurrences[(a.begin + i) % a.size]++;
			return 0;
		}
	}
	return 1;
}

/* Count the occurrences of all the letters in
 * the array */
unsigned int count_all(struct circular_array a)
{
	unsigned int i, sum = 0;

	for (i = 0; i < a.elements; ++i)
	{
		sum += a.occurrences[(a.begin + i) % a.size];
	}

	return sum;
}

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s k s\n\n", name);
	fprintf(stderr, "k\tthe number of occurrences to find\n");
	fprintf(stderr, "s\tthe input string\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "0 and the size of the longest substring in stdout.\n");
	fprintf(stderr, "a positive vaue in case there was an error.\n");
}

int main(int argc, char **argv)
{
	long k;
	char *endptr = NULL, *input_string;
	size_t input_length;

	unsigned int max_length = 0, curr_length, i;
	struct circular_array last_chars;

#ifdef __DEBUG__
	char *copied_substr = NULL, *start_dbg_ptr;
#endif

	// Parse arguments

	if (argc < 3)
	{
		fprintf(stderr, "ERROR: You must specify k and s.\n");
		print_help(argv[0]);
		exit(1);
	}

	errno = 0;
	k = strtol(argv[1], &endptr, 10);
	STRTOL_ERROR_HANDLING(argv[1], endptr, 1);

	input_string = argv[2];
	input_length = strlen(input_string);

#ifdef __DEBUG__
	printf("Input string is \"%s\".\n", input_string);
	printf("k is %ld.\n", k);
	start_dbg_ptr = input_string;
#endif

	// Init the data structures

	last_chars.array = malloc(sizeof(char)*k);
	if (last_chars.array == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(1);
	}
	last_chars.occurrences = malloc(sizeof(unsigned int)*k);
	if (last_chars.occurrences == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(1);
	}
	last_chars.size = k;
	last_chars.begin = 0;
	last_chars.elements = 0;

	// Algorithm

	for (i = 0; i < input_length; ++i)
	{
		if (check_new_or_count(last_chars, input_string[i]))
		{
			// The letter is new in the array, now check if we
			// reached the total capacity of the array (k) before
			// adding it
			if (last_chars.elements >= k)
			{
				// The array is already full, check if we reached a new
				// longest substring
				curr_length = count_all(last_chars);
				if (curr_length > max_length)
				{
					max_length = curr_length;
#ifdef __DEBUG__
					start_dbg_ptr = input_string + i - max_length;
#endif
				}
			}
			// Finally add the new letter
			add_new(&last_chars, input_string[i]);
		}
	}

	curr_length = count_all(last_chars);
	if (curr_length > max_length)
	{
		max_length = curr_length;
#ifdef __DEBUG__
		start_dbg_ptr = input_string + input_length - max_length;
#endif
	}

	printf("Longest substring with %ld distinct characters is long %u.\n", k, max_length);

#ifdef __DEBUG__
	copied_substr = malloc(sizeof(char)*max_length+1);
	if (copied_substr == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer while allocating copied_substr.\n");
		exit(1);
	}
	strncpy(copied_substr, start_dbg_ptr, max_length);
	copied_substr[max_length] = '\0';
	printf("The substring is \"%s\".\n", copied_substr);
	free(copied_substr);
#endif

	free(last_chars.array);
	free(last_chars.occurrences);

	return 0;
}
