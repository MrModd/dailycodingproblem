# Daily Coding Problem: Problem #3 [Medium]

This problem was asked by Google.

Given the root to a binary tree, implement `serialize(root)`, which serializes
the tree into a string, and `deserialize(s)`, which deserializes the string
back into the tree.

For example, given the following `Node` class

```
class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
```

The following test should pass:

```
node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'
```

## Solution

Convert the tree in an array representation where given the array `G` then
`G[i]` represent a node `i` in the graph. His left child is `G[2*i+1]` and
his right child is `G[2*i+2]`. The root of the graph is `G[0]`.

After building the array flatten it into a single string divided by commas.
To be sure commas are not part of the value of some nodes convert first the
values into base64 which are safer to handle.

To deserialize do the opposite.

### Execution time

Serialization includes the following passes:
- Breadth First Search (BFS) of the graph: `o(n)`.
- Conversion from array of strings to single string: `o(n)`.

Deserialization only requires a BFS: `o(n)`.

### Memory space

`o(n)` for both serialization and deserialization.
