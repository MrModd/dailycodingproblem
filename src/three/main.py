#!/usr/bin/env python

from binary_tree import Node
from serializer import serialize, deserialize, graph_to_string

def main():
    # Assignment
    node = Node('root', Node('left', Node('left.left')), Node('right'))
    assert deserialize(serialize(node)).left.left.val == 'left.left'

    # Extra
    node = Node('root', Node('left', Node('left.left')), Node('right', right=Node('right.right', right=Node('right.right.right'))))
    print("Initial graph:" + graph_to_string(node))
    print("Serialized graph: " + serialize(node))
    print("Unserialized graph: " + graph_to_string(deserialize(serialize(node))))


if __name__ == "__main__":
    main()
