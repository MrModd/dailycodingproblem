from binary_tree import Node
from base64 import standard_b64encode, standard_b64decode

def serialize(root):
    values = []
    nodes_to_visit = [ (0,root) ]

    while len(nodes_to_visit) > 0:
        current = nodes_to_visit.pop(0)
        index = current[0]
        node = current[1]

        if node.left != None:
            nodes_to_visit.append( ( (index*2)+1 , node.left) )
        if node.right != None:
            nodes_to_visit.append( ( (index*2)+2, node.right) )

        while len(values) < index:
            values.append('')
        values.append( standard_b64encode(node.val.encode()).decode() )

    return ','.join(values)



def deserialize(string):
    values = string.split(',')
    root = Node( standard_b64decode(values[0].encode()).decode() )
    nodes_to_visit = [ (0,root) ]

    while len(nodes_to_visit) > 0:
        current = nodes_to_visit.pop(0)
        index = current[0]
        node = current[1]

        if (index*2)+1 < len(values) and values[(index*2)+1] != '':
            node.left = Node( standard_b64decode(values[(index*2)+1].encode()).decode() )
            nodes_to_visit.append( ( (index*2)+1 , node.left )  )
        if (index*2)+2 < len(values) and values[(index*2)+2] != '':
            node.right = Node( standard_b64decode(values[(index*2)+2].encode()).decode() )
            nodes_to_visit.append( ( (index*2)+2 , node.right )  )

    return root

def graph_to_string(root):
    serial = serialize(root)
    values = serial.split(',')
    for i in range(len(values)):
        values[i] = standard_b64decode(values[i].encode()).decode()
    return str(values)
