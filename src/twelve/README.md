# Daily Coding Problem: Problem #12 [Hard]

This problem was asked by Amazon.

There exists a staircase with N steps, and you can climb up either 1 or 2
steps at a time. Given N, write a function that returns the number of
unique ways you can climb the staircase. The order of the steps matters.

For example, if N is 4, then there are 5 unique ways:

```
    1, 1, 1, 1
    2, 1, 1
    1, 2, 1
    1, 1, 2
    2, 2
```

What if, instead of being able to climb 1 or 2 steps at a time, you could
climb any number from a set of positive integers X? For example, if
X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time.

## Solution

The solution to this problem relies on the Fibonacci sequence.
Let's assume the example of the problem: 4 steps and the possibility
to take 1 or 2 steps at a time.

First solution is to make 1 step at every move. Then you start
considering making 2 steps in a movement. For this set of solution you
have to scale down the size of the problem from 4 to 3 and then
consider all the possible permutations: `[(2,1,1),(1,2,1),(1,1,2)]`.
Then you scale again the problem from 3 to 2. With 2 steps you can
only make one move: `[(2,2)]`. Remember we already considered the
case with only single steps movements.

In general if you have `n` steps and you can do movements of 1 or 2
steps at a time you can go upstairs with a number of ways that is
equal to the solutions for `n-1` steps + the new solutions with
the additional step that happen to be equal to the number of
solution for `n-2`. So `F(n) = F(n-1) + F(n-2)`.

It's easy to generalize this problem for the case you can make
moves of `{1, 2, 3, ..., n}` steps. If `n` equals `4` then the
number of solution for `k` stairs is `Tetranacci(4)` (which is
`T(4) = T(3) + T(2) + T(1) + T(0)`).

The problem asks what happens if you can make moves from a set
of defined steps such as `[1, 2, 4, 5]`. In this case I assume
it's always possible to make a single step move. In order to
exclude the solutions with `3` steps you just remove the the
difference in solutions that `3` adds in respect to the previous
number of steps in a move, in this case `2`.

So if you have `n` steps and you can only make moves of
`[1, 2, 5, 6]` steps the number of possible solutions will be
`6_bonacci(n) - (4_bonacci(n) - 2_bonacci(n))`. This exclude
solutions with `3` and `4` steps.

### Execution time

The simple case with `1` and `2` steps takes `o(n)` for the
execution of Fibonacci sequence until the `n-th` number.

Considering a set `k` of possible moves you may have to compute
Fibonacci up to `k` times until the `n-th` number, so `o(nk)`.
Also if the `k` numbers are not in order you have to order them,
so `o(nlog n)` to order the set f `k` possible moves, for a total
execution of `o(nlog n + nk)`.

### Memory space

We can consider that the algorithm runs with a memory size that
is proportional to k, so `o(k)`. But very likely `k` is a very
small number. We could almost assume that the memory space is
constant, `o(1)`.
