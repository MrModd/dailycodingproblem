#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/utils.h"
#include "../common/sort.h"
#include "../common/strtol_error_handling.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s N [M1, M2, ...]\n\n", name);
	fprintf(stderr, "N\t\tthe number of steps\n\n");
	fprintf(stderr, "M1,...\t\tthe allowed moves\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "0 and the number of possible climbings in stdout.\n");
	fprintf(stderr, "a positive vaue in case there was an error.\n");
}

/* Compute the n-th number of the k-bonacci sequence, where k=sequence. */
unsigned long long n_bonacci(unsigned long n, unsigned long sequence)
{
	unsigned long long *values;
	unsigned int i, j;

	if (sequence < 2)
	{
		// 1_bonacci(n) = 1 for every n
		// 0_bonacci(n) = 0 for every n
		 return sequence - 1;
	}

	if (n < 3)
	{
		return 1ull;
	}

	values = calloc(sizeof(unsigned long long), sequence);
	if (values == NULL)
	{
		fprintf(stderr, "ERROR: calloc returned a NULL pointer.\n");
		exit(1);
	}

	// Init accumulators
	values[0] = 1ull;
	values[1] = 1ull;
	for (i = 2; i < sequence; ++i)
	{
		for (j = 0; j < i; ++j)
		{
			values[i] += values[j];
		}
	}

	if (n <= sequence)
	{
		return values[n-1];
	}

	// Find n-bonacci(N)
	for (i = sequence; i < n; ++i)
	{
		unsigned long long tmp = values[0];
		for (j = 0; j < sequence - 1; ++j)
		{
			values[j] = values[j+1];
			tmp += values[j];
		}
		values[sequence-1] = tmp;
	}

#ifdef __DEBUG__
	printf("%lu_bonacci(%lu) = %llu\n", sequence, n, values[sequence-1]);
#endif

	return values[sequence-1];
}

int main(int argc, char **argv)
{
	long N, *M;
	unsigned int size;
	int i;
	unsigned long long solutions;
	char *endptr = NULL;

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify at least N.\n");
		print_help(argv[0]);
		exit(1);
	}

	errno = 0;
	N = strtol(argv[1], &endptr, 10);
	STRTOL_ERROR_HANDLING(argv[1], endptr, 1);

	if (argc == 2)
	{
		M = malloc(sizeof(long));
		size = 1;
	}
	else
	{
		size = argc-2;
		M = malloc(sizeof(long)*size);
	}
	if (M == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(1);
	}

	if (argc == 2)
	{
		M[0] = 2l;
	}
	else
	{
		parse_array_long(M, argv+2, argc-2);
	}

#ifdef __DEBUG__
	print_array(stdout, "Allowed steps", M, size);
	/*printf("Testing if fibonacci function returns expected results:\n");
	printf("Fibonacci 1: %llu, expected result: 1\n", n_bonacci(1, 2));
	printf("Fibonacci 2: %llu, expected result: 1\n", n_bonacci(2, 2));
	printf("Fibonacci 3: %llu, expected result: 2\n", n_bonacci(3, 2));
	printf("Fibonacci 4: %llu, expected result: 3\n", n_bonacci(4, 2));
	printf("Fibonacci 5: %llu, expected result: 5\n", n_bonacci(5, 2));
	printf("Fibonacci 6: %llu, expected result: 8\n", n_bonacci(6, 2));
	printf("Tribonacci 1: %llu, expected result: 1\n", n_bonacci(1, 3));
	printf("Tribonacci 2: %llu, expected result: 1\n", n_bonacci(2, 3));
	printf("Tribonacci 3: %llu, expected result: 2\n", n_bonacci(3, 3));
	printf("Tribonacci 4: %llu, expected result: 4\n", n_bonacci(4, 3));
	printf("Tribonacci 5: %llu, expected result: 7\n", n_bonacci(5, 3));
	printf("Tribonacci 6: %llu, expected result: 13\n", n_bonacci(6, 3));
	printf("Tetranacci 1: %llu, expected result: 1\n", n_bonacci(1, 4));
	printf("Tetranacci 2: %llu, expected result: 1\n", n_bonacci(2, 4));
	printf("Tetranacci 3: %llu, expected result: 2\n", n_bonacci(3, 4));
	printf("Tetranacci 4: %llu, expected result: 4\n", n_bonacci(4, 4));
	printf("Tetranacci 5: %llu, expected result: 8\n", n_bonacci(5, 4));
	printf("Tetranacci 6: %llu, expected result: 15\n", n_bonacci(6, 4));
	printf("Pentanacci 6: %llu, expected result: 16\n", n_bonacci(6, 5));
	printf("TEST COMPLETED.\n");*/
#endif

	// Order array of allowed steps
	sort(M, size);

	// To start compute the solutions with the highest number of steps
	solutions = n_bonacci(N+1, M[size-1]);

	// Then subtract the solutions with the intermediate steps that are
	// not allowed (e.g. steps are 2, 4, 5 -> remove the solutions with 3
	// steps)
	for (i = size-2; i >= 0; --i)
	{
		if (M[i]+1 < M[i+1])
		{
			// Exclude solutions from not allowed steps between M[i] and M[i+1]
			solutions -= n_bonacci(N+1, M[i+1]-1) - n_bonacci(N+1, M[i]);
		}
	}

	// Finally substract the solutions with steps under the lowest
	// number of allowed steps (e.g. steps are 5, 6, 7 -> remove solutions
	// with steps 2, 3, 4)
	if (M[0] > 2)
	{
		solutions -= n_bonacci(N+1, M[0]-1);
	}

	printf("Possible solutions: %llu.\n", solutions);

	return 0;
}
