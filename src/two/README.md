# Daily Coding Problem: Problem #2 [Hard]

This problem was asked by Uber.

Given an array of integers, return a new array such that each element at index `i` of
the new array is the product of all the numbers in the original array except the one at `i`.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be
`[120, 60, 40, 30, 24]`. If our input was `[3, 2, 1]`, the expected output would
be `[2, 3, 6]`.

Follow-up: what if you can't use division?

## Solution

Given an input array `a` of `n` elements.

You can build the output array `b` in two passes without divisions:

- **First pass**: in the general step `i`, `b[i]` must be equal to `b[i-1]*a[i-1]`.
  `b[0]` is initialized to `1`.
- **Second pass**: you scan the array in the other direction. In the general step `i`,
  `b[i] = b[i]*tmp` where `tmp` is equal to `a[i+1]*a[i+2]*...*a[n-1]`.

### Execution time

`o(2n)=o(n)`: Two scans of the array.

### Memory space

`o(2n)=o(n)`: One array to contain the input and one array to contain the output.
