#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../common/strtol_error_handling.h"

void print_help(char *name)
{
	fprintf(stderr, "USAGE: %s n1 n2 n3 ...\n\n", name);
	fprintf(stderr, "n1,n2,...\tthe numbers in the array\n\n");
	fprintf(stderr, "RETURN:\n");
	fprintf(stderr, "99 in case there was an error.\n");
	fprintf(stderr, "0 otherwise and the new array in the stdout.\n");
}

int main(int argc, char **argv)
{
	int i, size = argc-1;
	long *input, *output, tmp;
	char *endptr = 0;

	if (argc < 2)
	{
		fprintf(stderr, "ERROR: You must specify at least one element of the input array.\n");
		print_help(argv[0]);
		exit(99);
	}

	input = malloc(sizeof(long)*size);
	if (input == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(99);
	}
	output = malloc(sizeof(long)*size);
	if (output == NULL)
	{
		fprintf(stderr, "ERROR: malloc returned a NULL pointer.\n");
		exit(99);
	}

#ifdef __DEBUG__
	printf("Numbers: ");
#endif
	for(i = 1; i < argc; ++i) {
		errno = 0;
		input[i-1] = strtol(argv[i], &endptr, 10);
		STRTOL_ERROR_HANDLING(argv[i], endptr, 99);
#ifdef __DEBUG__
		printf("%ld ", input[i-1]);
#endif
	}
#ifdef __DEBUG__
	printf("\n");
#endif

	// First pass
	output[0] = 1;
	for(i = 1; i < size; ++i)
	{
		output[i] = output[i-1] * input[i-1];
	}

#ifdef __DEBUG__
	printf("Intermediate step: ");
	for (i = 0; i < size; ++i)
	{
		printf("%ld ", output[i]);
	}
	printf("\n");
#endif

	// Second pass (inverse scan)
	tmp = input[size-1];
	for (i = size-2; i >= 0; --i)
	{
		output[i] *= tmp;
		tmp *= input[i];
	}

	printf("Result: ");
	for (i = 0; i < size; ++i)
	{
		printf("%ld ", output[i]);
	}
	printf("\n");

	free(input);
	free(output);

	return 0;
}
